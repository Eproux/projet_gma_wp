﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Windows.Storage;


namespace WS_GMA_ELM
{
    class DAOTrou
    {

        private MySqlConnection conn;
        public DAOTrou()
        {
            string myConnectionString;
            //myConnectionString = "server=193.252.48.172;port=20433;uid=golfede;pwd=golfede;database=golfbdd;";
            myConnectionString = "server=172.18.207.102;port=3306;uid=golfede;pwd=golfede;database=golfbdd;";
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            conn.ConnectionString = myConnectionString;
            conn.Open();
        }

        /*

        public void recordATrou(Trou t)
        {
            String myInsertQuery = "insert into trou VALUES (" + t.Numero + "," + t.Distance + "," + t.Par + ",'" + ApplicationData.actuel.Code + "')";
            MySqlCommand myCommand = new MySqlCommand(myInsertQuery);
            myCommand.Connection = conn;
            myCommand.ExecuteNonQuery();
        }
         * */

        public List<Trou> getAllTrousFromClub(Club c)
        {
            List<Trou> lesTrousRecuperes = new List<Trou>();

            string requete = "select * from trou where code_club = '" + c.Code + "'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

          

            while (rdr.Read())
            {
                Trou t = new Trou();
                t.Numero = Convert.ToInt16(rdr[0].ToString());
                t.Distance = Convert.ToInt16(rdr[1].ToString());
                t.Par = Convert.ToInt16(rdr[2].ToString());

                lesTrousRecuperes.Add(t);
            }
            rdr.Close();
            return lesTrousRecuperes;
        }
    }
}
