﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;

namespace WS_GMA_ELM
{
    /// <summary>
    /// Description résumée de WS_GMA_ELM
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante. 
    // [System.Web.Script.Services.ScriptService]
    public class WS_GMA_ELM : System.Web.Services.WebService
    {

        [WebMethod]
        public string HelloWorld()
        {
         
            return "Hello World";
        }


        [WebMethod]
        public string GetClub(string Info)
        {

            List<Club> data = new List<Club>();

            DAOClub DaoClub = new DAOClub();

            data = DaoClub.GetClub(Info);

            JavaScriptSerializer j = new JavaScriptSerializer();


            return j.Serialize(data);

        }


        [WebMethod]
        public string Club()
        {

            List<Club> data = new List<Club>();

            DAOClub DaoClub = new DAOClub();

            data = DaoClub.getAllClubs();

            JavaScriptSerializer j = new JavaScriptSerializer();


            return j.Serialize(data);

        }

        [WebMethod]
        public string ClubByNom(string unNom)
        {

            List<Club> data = new List<Club>();

            DAOClub DaoClub = new DAOClub();

            data = DaoClub.getAClubByNom(unNom);

            JavaScriptSerializer j = new JavaScriptSerializer();


            return j.Serialize(data);

        }


        [WebMethod]
        public string ClubByVille(string uneVille)
        {

            List<Club> data = new List<Club>();

            DAOClub DaoClub = new DAOClub();

            data = DaoClub.getAClubByVille(uneVille);

            JavaScriptSerializer j = new JavaScriptSerializer();


            return j.Serialize(data);

        }

        [WebMethod]
        public string ClubByCp(string unCP)
        {

            List<Club> data = new List<Club>();

            DAOClub DaoClub = new DAOClub();

            data = DaoClub.getAClubByCp(unCP);

            JavaScriptSerializer j = new JavaScriptSerializer();


            return j.Serialize(data);

        }


        [WebMethod]
        public string GetTrou(Club c)
        {

            List<Trou> data = new List<Trou>();

            DAOTrou DAOTrou = new DAOTrou();

            data = DAOTrou.getAllTrousFromClub(c);

            JavaScriptSerializer j = new JavaScriptSerializer();


            return j.Serialize(data);
        }
    }
}
