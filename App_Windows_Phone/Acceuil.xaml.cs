﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Data.Json;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace App_Windows_Phone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class Acceuil : Page
    {
        public static  string JSONFILENAME = "data2.json";
        public static Carte LaCarte = null;
        public static List<Club> LesClubs = null;
        public static Club LeCLub = null;
        public static List<Trou> LesTrous = null;

        public Acceuil()
        {
            this.InitializeComponent();

        }


     



        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

         private void Button_Click(object sender, RoutedEventArgs e)
        {
          

           
        }

        static public async Task writeJsonAsync()
        {
            // Notice that the write is ALMOST identical ... except for the serializer.

            var serializer = new DataContractJsonSerializer(typeof(Carte));
            using (var stream = await ApplicationData.Current.LocalFolder.OpenStreamForWriteAsync(
                          JSONFILENAME,
                          CreationCollisionOption.ReplaceExisting))
            {
                serializer.WriteObject(stream, Acceuil.LaCarte);
            }


        }

        static public async Task deserializeJsonAsync()
        {
            bool fileExists = true;
            Stream myStream = null;
            StorageFile file = null;

            string content = String.Empty;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync(JSONFILENAME);
                myStream = await file.OpenStreamForReadAsync();
                myStream.Dispose();

                Carte laCarte = null;

                var jsonSerializer = new DataContractJsonSerializer(typeof(Carte));

                myStream = await ApplicationData.Current.LocalFolder.OpenStreamForReadAsync(JSONFILENAME);

                laCarte = (Carte)jsonSerializer.ReadObject(myStream);

                Acceuil.LaCarte = laCarte;
            }
            catch (FileNotFoundException)
            {
                // If the file dosn't exits it throws an exception, make fileExists false in this case 
                fileExists = false;
                content = "Fichier de sauvegarde inexistant.";
            }
            finally
            {
                if (myStream != null)
                {
                    myStream.Dispose();
                }
            }

        }

        async private void OnLinkTapped(object sender, TappedRoutedEventArgs e)
        {

            TextBlock txt = (TextBlock)sender;//.Parent.FindControl("tbReason")
            StackPanel mtt = (StackPanel)((StackPanel)txt.Parent).Parent;


            var _Child = VisualTreeHelper.GetChild(mtt, 0);
            TextBlock t = (TextBlock)VisualTreeHelper.GetChild(_Child, 1);
            //  System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - INFO : RELAISE -->" + txt.Text + " sur le trou " + t.Text);


            /////////////////////////////

            await Acceuil.deserializeJsonAsync();

            if (Acceuil.LaCarte == null)
            {
                string ladate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                foreach (Club unClub in Acceuil.LesClubs)
                {

                    if (unClub.Code == t.Text)
                    {
                        Acceuil.LeCLub = unClub;
                    }


                    Acceuil.LesTrous = null;
                }

                Carte carte = new Carte(ladate, 0, "", Acceuil.LeCLub, MainPage.leJoueur);
                Acceuil.LaCarte = carte;

                await Acceuil.writeJsonAsync();

                this.Frame.Navigate(typeof(ListeTrou));
            }
            else
            {
                this.Frame.Navigate(typeof(ListeTrou));
            }
        }

       


        private void T_SearchNom(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ListCLub_Nom));
        }

        private void T_SearchCp(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ListCLub_Cp));
        }

        private void T_SearchVille(object sender, TappedRoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ListCLub_Ville));
        }

        private void SearchClub(object sender, RoutedEventArgs e)
        {

          /*  var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/GetClub");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>  <GetClub xmlns=\"http://tempuri.org/\"><Info>" + tb_SearchClub.Text + "</Info></GetClub></soap:Body></soap:Envelope>";
            var response = httpClient.PostAsync("http://localhost:3990/WS_GMA_ELM.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);*/


           
            

            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
            httpClient.DefaultRequestHeaders.Add("SOAPAction", "http://tempuri.org/GetClub");
            var soapXml = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body><GetClub xmlns=\"http://tempuri.org/\"><Info>" + tb_SearchClub.Text + "</Info></GetClub></soap:Body></soap:Envelope>";
            
            var response = httpClient.PostAsync("http://172.18.207.204:8083/WS_GMA_ELM.asmx", new StringContent(soapXml, Encoding.UTF8, "text/xml")).Result;
            var content = response.Content.ReadAsStringAsync().Result;
            XDocument responseXML = XDocument.Parse(content);
            String contenu = responseXML.Root.Value;
            System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE : " + contenu);

            if (contenu == "[]")
            {
                
                message.Text = "Aucun Club n'a été trouvé";

            }
            else
            {

                message.Text = "";

                var jsonSerializer = new DataContractJsonSerializer(typeof(List<Club>));
                var stream = new MemoryStream(Encoding.UTF8.GetBytes(contenu));
                Acceuil.LesClubs = (List<Club>)jsonSerializer.ReadObject(stream);


                System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - INFO : RELAISE2 -->" + MainPage.leJoueur.login);
                System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - INFO : RELAISE2 -->" + MainPage.leJoueur.mdp);

                foreach (Club club in Acceuil.LesClubs)
                {

                    using (var httpClient2 = new HttpClient())
                    {
                        // classe utilisée pour sérialiser des instances en JSON - ici une List de Note est spécifié pour indiquer les data à sérialiser.
                        var serializer = new DataContractJsonSerializer(typeof(Club));
                        // flux dont le contenu est en mémoire
                        var stream_post = new MemoryStream();
                        // sérialise la collection de notes et la copie dans le stream en mémoire.
                        serializer.WriteObject(stream_post, club);
                        // on se cale au début du flux
                        stream_post.Position = 0;
                        // on crée un flux de lecture à partir de celui en mémoire
                        StreamReader sr = new StreamReader(stream_post);


                        // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
                        httpClient2.BaseAddress = new Uri("http://localhost/projetGma2/");
                        httpClient2.DefaultRequestHeaders.Accept.Clear();
                        httpClient2.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                        string chaineTransmise = sr.ReadToEnd();


                        // exemple montrant un envoi de données clés valeurs sous forme de texte
                        var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Club",chaineTransmise),
                    new KeyValuePair<string, string>("Login", MainPage.leJoueur.login),
                    new KeyValuePair<string, string>("Mdp", MainPage.leJoueur.mdp),
                    new KeyValuePair<string, string>("Action", "AjoutCLub")
                    
                };
                        // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                        var content2 = new FormUrlEncodedContent(pairs);

                        // formule l'appel au script distant en lui passant les données en POST
                        HttpResponseMessage response2 = httpClient2.PostAsync("script.php", content2).Result;

                        //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;


                    }
                }

                this.liste_des_Clubs.ItemsSource = Acceuil.LesClubs;





            }
        }


        

        private void Effacer(object sender, RoutedEventArgs e)
        {
            if (tb_SearchClub.Text == "Ville/CP/Nom")
            {
                tb_SearchClub.Text = "";
            }
        }



    }
}
