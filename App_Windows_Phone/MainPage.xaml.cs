﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Le modèle Application WebView est documenté à l'adresse http://go.microsoft.com/fwlink/?LinkID=391641

namespace App_Windows_Phone
{
    public sealed partial class MainPage : Page
    {
        private const string JSONFILENAME = "data.json";
        public static Joueur leJoueur = null;
        
     
        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
           

            HardwareButtons.BackPressed += this.MainPage_BackPressed;
        }

        /// <summary>
        /// Appelé quand l’utilisateur quitte la page.
        /// </summary>
        /// <param name="e">Données d’événement qui décrivent la navigation sur la page.</param>
        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            HardwareButtons.BackPressed -= this.MainPage_BackPressed;
        }

        /// <summary>
        /// Remplace l’appui sur le bouton Retour pour se déplacer dans la pile arrière du WebView, au lieu de celle de l’application.
        /// </summary>
        private void MainPage_BackPressed(object sender, BackPressedEventArgs e)
        {
           
        }

        private void Browser_NavigationCompleted(WebView sender, WebViewNavigationCompletedEventArgs args)
        {
            if (!args.IsSuccess)
            {
                Debug.WriteLine("Navigation to this page failed, check your internet connection.");
            }
        }

        /// <summary>
        /// Se déplace vers l’avant dans l’historique du WebView.
        /// </summary>
        private void ForwardAppBarButton_Click(object sender, RoutedEventArgs e)
        {
           
        }

        /// <summary>
        /// Se déplace vers la page d’accueil initiale.
        /// </summary>
        private void HomeAppBarButton_Click(object sender, RoutedEventArgs e)
        {
           
        }

        async private void Button_Click(object sender, RoutedEventArgs e)
        {
            string login = tb_Login.Text;
            string mdp = tb_Mdp.Text;





            using (var httpClient = new HttpClient())
            {
                /**********************************************************************************
                * PARTIE AVEC PROXY (A COMMENTER SI SCRIPT PHP AU LYCEE
                * BEGIN
                * ********************************************************************************/

                /*
                HttpClientHandler handler = new HttpClientHandler()
                {
                    Proxy = new WebProxy("http://192.168.100.254:3128"),
                    UseProxy = true,
                };

                // ... Use HttpClient.            
                HttpClient client = new HttpClient(handler);

                var byteArray = Encoding.ASCII.GetBytes("username:password1234");
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                */
                 
                /**********************************************************************************
                * PARTIE AVEC PROXY (A COMMENTER SI SCRIPT PHP AU LYCEE
                * END
                * ********************************************************************************/

                // classe utilisée pour sérialiser des instances en JSON - ici une List de Note est spécifié pour indiquer les data à sérialiser.
                // flux dont le contenu est en mémoire
                var stream_post = new MemoryStream();
                // sérialise la collection de notes et la copie dans le stream en mémoire.
                // on se cale au début du flux
                stream_post.Position = 0;
                // on crée un flux de lecture à partir de celui en mémoire
                StreamReader sr = new StreamReader(stream_post);

                // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
              //  httpClient.BaseAddress = new Uri("http://localhost/projetGma2/");
                httpClient.BaseAddress = new Uri("http://projetgma.esy.es/script_ws/");

                
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                // exemple montrant un envoi de données clés valeurs sous forme de texte
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Lelogin", login),
                    new KeyValuePair<string, string>("LeMdp", mdp),
                    new KeyValuePair<string, string>("Action", "Connexion")
                };
                // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                var content = new FormUrlEncodedContent(pairs);

                // formule l'appel au script distant en lui passant les données en POST
                HttpResponseMessage response = httpClient.PostAsync("script.php", content).Result;

                //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                string statusCode = response.StatusCode.ToString();

                response.EnsureSuccessStatusCode();
                Task<string> responseBody = response.Content.ReadAsStringAsync();

                var jsonSerializer = new DataContractJsonSerializer(typeof(Joueur));
                var stream = new MemoryStream(Encoding.UTF8.GetBytes(responseBody.Result));
                Joueur j = (Joueur)jsonSerializer.ReadObject(stream);

               

                if (j.Libelle != "Erreur d'authentification")
                {
                   

                    await Acceuil.deserializeJsonAsync();

                    if (Acceuil.LaCarte != null)
                    {
                        this.Frame.Navigate(typeof(ListeTrou));
                    }
                    else
                    {

                        MainPage.leJoueur = j;

                        this.Frame.Navigate(typeof(Acceuil));
                    }
                }
                else
                {


                    tb_resultat.Text = j.Libelle;
                }

            }

           
            

         

            
        }

        private void tb_Mdp_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
