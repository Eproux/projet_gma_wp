﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


// Pour en savoir plus sur le modèle d’élément Page vierge, consultez la page http://go.microsoft.com/fwlink/?LinkID=390556

namespace App_Windows_Phone
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class ListeTrou : Page
    {
        public ListeTrou()
        {
            this.InitializeComponent();

            List<Trou> LesTrous = new List<Trou>();

           LesTrous = Acceuil.LaCarte.leParcours.getLesTrous();
            List<Trou> LesBonTrous = new List<Trou>();

            System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - INFO : RELAISE1 -->" + LesTrous);

            foreach(Trou T in LesTrous){

                System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - INFO : RELAISE2 -->" + T);

                if(T.ScoreJoueur == 0){

                    LesBonTrous.Add(T) ;


                }

            }

            this.liste_des_trou.ItemsSource = LesBonTrous;
        
            
        }

        /// <summary>
        /// Invoqué lorsque cette page est sur le point d'être affichée dans un frame.
        /// </summary>
        /// <param name="e">Données d'événement décrivant la manière dont l'utilisateur a accédé à cette page.
        /// Ce paramètre est généralement utilisé pour configurer la page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }


       async private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = (TextBox)sender;//.Parent.FindControl("tbReason")
            StackPanel mtt = (StackPanel)((StackPanel)txt.Parent).Parent;
       
            //for (int i = 0; i < VisualTreeHelper.GetChildrenCount(mtt); i++)
            //{
            var _Child = VisualTreeHelper.GetChild(mtt, 0);
            TextBlock t = (TextBlock)VisualTreeHelper.GetChild(_Child,1);
            System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - INFO : RELAISE -->" + txt.Text + " sur le trou " + t.Text);
            //}



           int score = Acceuil.LaCarte.score + Convert.ToInt16(txt.Text) ;
           int idTrou = Convert.ToInt16(t.Text);

           //////////////

            // ON RECUPERE LES TROU DU CLUB AVEC SON CODE

                       

                        Acceuil.LesTrous = Acceuil.LaCarte.leParcours.getLesTrous();

                        
                         foreach(Trou unTrou in Acceuil.LesTrous){


                             if (unTrou.Numero == idTrou)
                             {

                                  unTrou.ScoreJoueur=Convert.ToInt16(txt.Text);
                             }
                            
                         }


                        Acceuil.LaCarte.leParcours.setLesTrous(Acceuil.LesTrous);
                        

                       

           /////////////


           Carte NewCarte = new Carte(Acceuil.LaCarte.dateCarte, score,"", Acceuil.LaCarte.leParcours, Acceuil.LaCarte.leJoueur);

             
           Acceuil.LaCarte = NewCarte ;

           await Acceuil.writeJsonAsync();

         
        }


         async private void Button_Click(object sender, RoutedEventArgs e)
        {
            

     
            int ScoreTotal = Acceuil.LaCarte.score;
             int TotalPar=0;

             foreach(Trou t in Acceuil.LaCarte.leParcours.getLesTrous()){

                 TotalPar = t.Par;

             }

            if(ScoreTotal == TotalPar){
	
		        Acceuil.LaCarte.commentaire ="Bien vous avez fait un parcours correcte";
	
	        }
	
	       if(ScoreTotal < TotalPar){
	
		        Acceuil.LaCarte.commentaire ="Exellent vous avez fait un parcours parfait";
	
	        }
	
	
	        if(ScoreTotal > TotalPar){
	
		        Acceuil.LaCarte.commentaire ="Vous êtes vraiment nul";
	
	        }


	
            using (var httpClient = new HttpClient())
            {
                // classe utilisée pour sérialiser des instances en JSON - ici une List de Note est spécifié pour indiquer les data à sérialiser.
                var serializer = new DataContractJsonSerializer(typeof(Carte));
                // flux dont le contenu est en mémoire
                var stream_post = new MemoryStream();
                // sérialise la collection de notes et la copie dans le stream en mémoire.
                serializer.WriteObject(stream_post, Acceuil.LaCarte);
                // on se cale au début du flux
                stream_post.Position = 0;
                // on crée un flux de lecture à partir de celui en mémoire
                StreamReader sr = new StreamReader(stream_post);

                // on spécifie l'adresse web de la ressource à atteindre et on prépare l'entête http en tant qu'appli json
               // httpClient.BaseAddress = new Uri("http://localhost/projetGma2/");
                httpClient.BaseAddress = new Uri("http://projetgma.esy.es/script_ws/");
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                string chaineTransmise = sr.ReadToEnd();

                System.Diagnostics.Debug.WriteLine("DEBUG MESSAGE - " + chaineTransmise);

                // exemple montrant un envoi de données clés valeurs sous forme de texte
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("Carte",chaineTransmise),
                    new KeyValuePair<string, string>("Login", MainPage.leJoueur.login),
                    new KeyValuePair<string, string>("Mdp", MainPage.leJoueur.mdp),
                    new KeyValuePair<string, string>("Action", "AjoutCarte")
                    
                };
                // on encode le tout au format url (clé=valeur&cle2=valeur2 ...)
                var content = new FormUrlEncodedContent(pairs);

                // formule l'appel au script distant en lui passant les données en POST
                HttpResponseMessage response = httpClient.PostAsync("script.php", content).Result;

                //HttpResponseMessage response = httpClient.PostAsync("ws_phone_test1.php", new StringContent(sr.ReadToEnd(), Encoding.UTF8, "application/json")).Result;

                
            }
             


            StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync(Acceuil.JSONFILENAME);

            if (filed != null)
            {
                await filed.DeleteAsync();
            }

         

            this.Frame.Navigate(typeof(PageEnregistrement));
        }

         async private void Button_Click_1(object sender, RoutedEventArgs e)
         {
             StorageFile filed = await ApplicationData.Current.LocalFolder.GetFileAsync(Acceuil.JSONFILENAME);

             if (filed != null)
             {
                 await filed.DeleteAsync();
             }

         Acceuil.LaCarte = null;
         Acceuil.LesClubs = null;
         Acceuil.LeCLub = null;
         Acceuil.LesTrous = null;

         this.Frame.Navigate(typeof(Acceuil));
         }
    }
}
