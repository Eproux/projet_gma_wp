﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App_Windows_Phone
{
   public class Carte
    {

 
        public string dateCarte { get; set; }
        public int score { get; set; }
        public string commentaire { get; set; }

        public Club leParcours { get; set; }
        public Joueur leJoueur { get; set; }

        public string NomParcours { get { return leParcours.Nom ; } }
        public string AdresseParcours { get { return leParcours.Adresse; } }

        public string ScoreCarte { get { return "Score : "+score ;} }

        public Carte(string uneDateCarte, int unScore, string unCommentaire, Club unClub, Joueur unJoueur) 
        {   
             dateCarte = uneDateCarte;
             score = unScore;
             commentaire = unCommentaire;
             leParcours = unClub;
             leJoueur = unJoueur;    
        }

        public Carte()
        {
        }

        public int getScore()
        {
            
            return score;
        }
    }
}
