﻿using App_Windows_Phone;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Web.Script.Serialization;



namespace WebService
{


[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[System.ComponentModel.ToolboxItem(false)]
// Pour autoriser l'appel de ce service Web depuis un script à l'aide d'ASP.NET AJAX, supprimez les marques de commentaire de la ligne suivante.
[System.Web.Script.Services.ScriptService]


public class WebServiceGMA : System.Web.Services.WebService
{

                [WebMethod]
                public string Club()
                {

                        List<Club> data = new List<Club>();

                        DAOClub DaoClub = new DAOClub();

                        data = DaoClub.getAllClubs();

                        JavaScriptSerializer j = new JavaScriptSerializer();
        
        
                        return j.Serialize(data);

                }

                [WebMethod]
                public string Trou(Club c)
                {

                    List<Trou> data = new List<Trou>();

                    DAOTrou DAOTrou = new DAOTrou();

                    data = DAOTrou.getAllTrousFromClub(c);

                    JavaScriptSerializer j = new JavaScriptSerializer();


                    return j.Serialize(data);
                }



}

   
}
    