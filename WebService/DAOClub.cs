﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;

namespace App_Windows_Phone
{
    class DAOClub
    {

        private MySqlConnection conn;
        public DAOClub()
        {
            string myConnectionString;
            //myConnectionString = "server=193.252.48.172;port=20433;uid=golfede;pwd=golfede;database=golfbdd;";
            myConnectionString = "server=172.18.207.102;port=3306;uid=golfede;pwd=golfede;database=golfbdd;";
            conn = new MySql.Data.MySqlClient.MySqlConnection();
            conn.ConnectionString = myConnectionString;
            conn.Open();
        }

        public void recordAClub(Club c)
        {
            // Utile pour gérer le problème de virgule qui devient point pour Mysql selon les infos de Globalization
            NumberFormatInfo nfi = new CultureInfo("fr-FR", false).NumberFormat;
            nfi.NumberDecimalSeparator = ".";
            // requête insert
            String myInsertQuery = "insert into club VALUES ('" + c.Code + "','" + c.Nom + "','" + c.Rue + "','" + c.Cp + "', '" + c.Ville + "','" + c.Gps_lat.ToString(nfi) + "','" + c.Gps_lon.ToString(nfi) + "','" + c.Tel + "','" + c.Email + "'," + c.Licencie + ")";
            MySqlCommand myCommand = new MySqlCommand(myInsertQuery);
            myCommand.Connection = conn;
            myCommand.ExecuteNonQuery();
        }

        public void deleteAClub(Club c)
        {
            String myQuery = "delete from club where code = '" + c.Code + "'";
            MySqlCommand myCommand = new MySqlCommand(myQuery);
            myCommand.Connection = conn;
            myCommand.ExecuteNonQuery();
        }

        public List<Club> getAllClubs()
        {
            List<Club> lesClubsRecuperes = new List<Club>();

            string requete = "select * from club";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            while (rdr.Read())
            {
                Club c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());

                DAOTrou daot = new DAOTrou();
                c.setLesTrous(daot.getAllTrousFromClub(c));

                lesClubsRecuperes.Add(c);
            }
            rdr.Close();
            return lesClubsRecuperes;
        }

        public Club getAClubById(string id)
        {
            Club c = null;
            string requete = "select * from club where code = '" + id + "'";

            MySqlCommand cmd = new MySqlCommand(requete, conn);
            MySqlDataReader rdr = cmd.ExecuteReader();

            if (rdr.Read())
            {
                c = new Club();
                c.Code = rdr[0].ToString();
                c.Nom = rdr[1].ToString();
                c.Rue = rdr[2].ToString();
                c.Cp = rdr[3].ToString();
                c.Ville = rdr[4].ToString();
                c.Gps_lat = Convert.ToDouble(rdr[5].ToString());
                c.Gps_lon = Convert.ToDouble(rdr[6].ToString());
                c.Tel = rdr[7].ToString();
                c.Email = rdr[8].ToString();
                c.Licencie = Convert.ToInt16(rdr[9].ToString());


            }
            rdr.Close();
            return c;
        }

    }
}
