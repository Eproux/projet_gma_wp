﻿using System;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using App_Windows_Phone;
using System.Collections.Generic;

namespace UnitTestAppGMA
{

    
    [TestClass]
    public class UnitTest1
    {
        /**
        * Méthode de test :  TestMethodCoherenceScoreTrousCarte
        * Date de création : 18/03/2015
        * Créateur : Loïc Bourgeois
        * Objectif : Vérification du bon fonctionnement du score de la carte par rapport à un parcours effectué
        * */
        /*[TestMethod]
        public void TestMethodCoherenceScoreTrousCarte()
        {
            Club c = new Club();
            c.Code = "1";
            c.Cp = "55000";
            c.Email = "test@test.com";
            c.Gps_lat = 0.0;
            c.Gps_lon = 0.0;
            c.Licencie = 50;
            c.Nom = "CLUB DE GOLF";
            c.Rue = "quelque part ...";
            c.Tel = "liste rouge";
            c.Ville = "BAR LE DUC";

            Trou t1 = new Trou(1, 200, 3);
            Trou t2 = new Trou(2, 200, 5);
            Trou t3 = new Trou(3, 503, 7);

            c.ajouterTrou(t1);
            c.ajouterTrou(t2);
            c.ajouterTrou(t3);

            Carte laCarte = new Carte();

            laCarte.commentaire = "parcours sous la pluie";
            laCarte.dateCarte = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            laCarte.leJoueur = new Joueur();
            

            t1.ScoreJoueur = 8;
            t2.ScoreJoueur = 1;
            t3.ScoreJoueur = 3;

            int Score = laCarte.getScore();

            Assert.AreEqual(12, Score ,"Test sur la valeur du score de la carte");
            
        }*/


        [TestMethod]
        public void TestAjoutListTrou()
        {
            Club c = new Club();
            c.Code = "1";
            c.Cp = "55000";
            c.Email = "test@test.com";
            c.Gps_lat = 0.0;
            c.Gps_lon = 0.0;
            c.Licencie = 50;
            c.Nom = "CLUB DE GOLF";
            c.Rue = "quelque part ...";
            c.Tel = "liste rouge";
            c.Ville = "BAR LE DUC";

            Trou t1 = new Trou(1, 200, 3);
            Trou t2 = new Trou(2, 200, 5);
            Trou t3 = new Trou(3, 503, 7);

            c.ajouterTrou(t1);
            c.ajouterTrou(t2);
            c.ajouterTrou(t3);

            List<Trou> LesTrous = c.getLesTrous();
            int count = LesTrous.Count;

            Assert.AreEqual(count, 3, "La méthode ajouterTrou ne marche pas");

        }


        [TestMethod]
        public void TestAjoutListTrou2()
        {
            Club c = new Club();
            c.Code = "1";
            c.Cp = "55000";
            c.Email = "test@test.com";
            c.Gps_lat = 0.0;
            c.Gps_lon = 0.0;
            c.Licencie = 50;
            c.Nom = "CLUB DE GOLF";
            c.Rue = "quelque part ...";
            c.Tel = "liste rouge";
            c.Ville = "BAR LE DUC";

            Trou t1 = new Trou(1, 200, 3);
            Trou t2 = new Trou(2, 200, 5);
            Trou t3 = new Trou(3, 503, 7);

            List<Trou> LesTrous = new List<Trou>();

            LesTrous.Add(t1);
            LesTrous.Add(t2);
            LesTrous.Add(t3);

            c.setLesTrous(LesTrous);

            List<Trou> T = c.getLesTrous();
            int count = T.Count;

            Assert.AreEqual(count, 3, "La méthode setLesTrous ne marche pas");

        }
        
    }
}
